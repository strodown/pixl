################################################################################
#
# vulkan-headers
#
################################################################################

VULKAN_HEADERS_VERSION = v1.3.231
VULKAN_HEADERS_SITE = $(call github,KhronosGroup,Vulkan-Headers,$(VULKAN_HEADERS_VERSION))
VULKAN_HEADERS_DEPEDENCIES = vulkan-samples vulkan-tools vulkan-loader
VULKAN_HEADERS_LICENSE = Apache-2.0
VULKAN_HEADERS_LICENSE_FILES = LICENSE.txt
VULKAN_HEADERS_INSTALL_STAGING = YES

$(eval $(cmake-package))
