#!/bin/bash

# This file runs migration scripts at each boot.
#
# Because they are run more than once, each migration must be *idempotent*.
# Running them at each boot ensures that even configurations stored on external
# USB drives will get migrated too.
#
# TODO: each migration should be in a separate timestamped file to ensure they
#       are executed in order and properly isolated. So far, they are just
#       declared in order, and assigned an incremental index (the currently
#       informative timestamp is generated with `date +%Y%m%d%H%M%S`).

# Run this script on startup only
if [[ $1 != "start" ]]; then
  exit 0
fi

# Useful global variables
_SHAREINIT="/recalbox/share_init"
_SHARE="/recalbox/share"
_ARCH=$(cat /recalbox/recalbox.arch)
INIT_SCRIPT=$(basename "$0")

# Significant files paths
recalboxConfPath="system/recalbox.conf"
esInputCfgPath="system/.pegasus-frontend/input.cfg"

##########################
###     Migrations     ###
##########################

recallog -s "${INIT_SCRIPT}" -t "MIGRATION" "Running MIGRATIONS..."

### Migration 6 [20210617165703] Keep it until 8.1
### Copy all psp saves to the new directory : see https://gitlab.com/recalbox/recalbox/-/issues/1773
PSPSAVEDIR="/recalbox/share/saves/psp/ppsspp/PSP/"
mkdir -p "${PSPSAVEDIR}"

for SAVE_DIR in "SAVEDATA" "PPSSPP_STATE"; do
  if [ ! -z "$(ls -A /recalbox/share/system/configs/ppsspp/PSP/${SAVE_DIR}/)" ]; then
    recallog -s "${INIT_SCRIPT}" -t "MIGRATION-INFO" "MIGRATION of PPSSPP saves from /recalbox/share/system/configs/ppsspp/PSP/${SAVE_DIR}/ to /recalbox/share/saves/psp/ppsspp/PSP/"
    cp -r "/recalbox/share/system/configs/ppsspp/PSP/${SAVE_DIR}/" "${PSPSAVEDIR}" && \
    rm -rf "/recalbox/share/system/configs/ppsspp/PSP/${SAVE_DIR}/"
  fi
done

### Migration 7 [20220323142920] Added in 8.1
### Rename all Amiga bios following commit cd57bb7bcc3c65392f15a1b9f19c5c9799400be4
AMIGABIOS=(kick33180.A500.rom kick34005.A500.rom kick37175.A500.rom kick40063.A600.rom kick02019.AROS.ext.rom kick02019.AROS.rom kick39106.A1200.rom kick40068.A1200.rom kick40068.A4000.rom kick34005.CDTV.rom kick40060.CD32.rom kick40060.CD32.ext.rom)

for file in "${AMIGABIOS[@]}"; do
  if [ -f "${_SHARE}/bios/${file}" ]; then
    recallog -s "${INIT_SCRIPT}" -t "MIGRATION-INFO" "MIGRATION of AMIGA bios - renaming ${_SHARE}/bios/${file}"
    mv "${_SHARE}/bios/${file}" "${_SHARE}/bios/${file%.rom}"
  fi
done