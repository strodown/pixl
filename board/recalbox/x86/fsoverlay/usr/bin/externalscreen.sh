# layout screen by Strodown
#!/bin/bash

export DISPLAY=:0
xrandrOutput=$(xrandr)

## Get info from xrandr
## Show all output and your state ( connected/disconnected )
allOutputs=$(echo "${xrandrOutput}" | awk '/connected/{print $1,$2}')

## Show only connected monitor 
connectedOutputs=$(echo "${allOutputs}" | awk '$2 == "connected"{print $1}')

## List all resolutions for all outputs
listOututResolution=$(echo "${xrandrOutput}" | awk -v 'monitor=\"^%1 connected\"' '/connected/ {p = 0} $0 ~ monitor {p = 1} p' | awk '{if(NR>1)print $1}')

## Check selected output resolution if not more 1080p
OUTPUT_MAXRES=$(echo "${xrandrOutput}" | awk '$1!~"[0-9]*x[0-9]*i?" {if (MAXRES!="") {print OUTPUT ";" MAXRES ";" FALLBACKRES;} OUTPUT=$1; MAXRES=""; FALLBACKRES=""; NBRESO=1} $1~"[0-9]*x[0-9]*i?" {if (NBRESO==1) {MAXRES=$1; NBRESO++;} HEIGHT=$1; gsub(/^[0-9]+x/,"",HEIGHT); if ((FALLBACKRES=="") && (HEIGHT+0<=1080)) {FALLBACKRES=$1;}} END {if (MAXRES!="") {print OUTPUT ";" MAXRES ";" FALLBACKRES;}}')

## Prefered output screen and force resolution from recalbox.conf
GAME_SCREEN=$(recalbox_settings -command load -key system.primary.screen)
GAME_SCREEN_RES=$(recalbox_settings -command load -key system.primary.screen.resolution)
GAME_SCREEN_FREQ=$(recalbox_settings -command load -key system.primary.screen.frequency)
GAME_SCREEN_ROTATION=$(recalbox_settings -command load -key system.primary.screen.rotation)

## Prefered output Marquee screen and force resolution from recalbox.conf
MARQUEE_SCREEN_ENABLED=$(recalbox_settings -command load -key system.secondary.screen.enabled)
MARQUEE_SCREEN=$(recalbox_settings -command load -key system.secondary.screen)
MARQUEE_SCREEN_RES=$(recalbox_settings -command load -key system.secondary.screen.resolution)
MARQUEE_SCREEN_FREQ=$(recalbox_settings -command load -key system.secondary.screen.frequency)
MARQUEE_SCREEN_POSITION=$(recalbox_settings -command load -key system.secondary.screen.position)
MARQUEE_SCREEN_ROTATION=$(recalbox_settings -command load -key system.secondary.screen.rotation)

echo "List all ouputs and states:"
echo "$allOutputs"
echo "list of connected monitor(s) :"
echo "$connectedOutputs"
echo "selected in recalbox.conf:"
echo "Game Screen: $GAME_SCREEN; $GAME_SCREEN_RES; $GAME_SCREEN_FREQ; $GAME_SCREEN_ROTATION"
echo "Marquee Screen: $MARQUEE_SCREEN_ENABLED; $MARQUEE_SCREEN_POSITION; $MARQUEE_SCREEN; $MARQUEE_SCREEN_RES; $MARQUEE_SCREEN_FREQ; $MARQUEE_SCREEN_ROTATION"
echo "$listOututResolution"

## prepare output arguments for Game Screen (resolution, freq, rotation)
if [[ -n ${GAME_SCREEN_RES} ]] && [[ -n ${GAME_SCREEN_FREQ} ]] && [[ -n ${GAME_SCREEN_ROTATION} ]]; then
  setResolution="--mode ${GAME_SCREEN_RES} --refresh ${GAME_SCREEN_FREQ} --rotate ${GAME_SCREEN_ROTATION}"

elif [[ -n ${GAME_SCREEN_RES} ]] && [[ -n ${GAME_SCREEN_FREQ} ]]; then
  setResolution="--mode ${GAME_SCREEN_RES} --refresh ${GAME_SCREEN_FREQ}"

elif [[ -n ${GAME_SCREEN_RES} ]]; then 
  setResolution="--mode ${GAME_SCREEN_RES}"
fi

## prepare output arguments for Marquee Screen (resolution, freq, rotation)
if [[ -n ${MARQUEE_SCREEN_RES} ]] && [[ -n ${MARQUEE_SCREEN_FREQ} ]] && [[ -n ${MARQUEE_SCREEN_ROTATION} ]]; then
  setMarqueeResolution="--mode ${MARQUEE_SCREEN_RES} --refresh ${MARQUEE_SCREEN_FREQ} --rotate ${MARQUEE_SCREEN_ROTATION}"

elif [[ -n ${MARQUEE_SCREEN_RES} ]] && [[ -n ${MARQUEE_SCREEN_FREQ} ]]; then
  setMarqueeResolution="--mode ${MARQUEE_SCREEN_RES} --refresh ${MARQUEE_SCREEN_FREQ}"

elif [[ -n ${MARQUEE_SCREEN_RES} ]]; then
  setMarqueeResolution="--mode ${MARQUEE_SCREEN_RES}"
fi

## Test if game screen is connected
if [[ -n "${GAME_SCREEN}" ]]; then
  GAME_SCREEN_OUTPUT=$(echo "${connectedOutputs}" | grep "${GAME_SCREEN}" | awk 'BEGIN {FS=";"} {print $1}')
  if [[ -n "${GAME_SCREEN}" ]] && [[ -n "${GAME_SCREEN_OUTPUT}" ]]; then
    GAME_SCREEN_CONNECTED="${GAME_SCREEN_OUTPUT}"
  fi
fi

## Check marquee screen enabled
if [[ "${MARQUEE_SCREEN_ENABLED}" == "1" ]]; then
  ## Test if marquee screen is connected
  if [[ -n "${MARQUEE_SCREEN}" ]]; then
    MARQUEE_SCREEN_OUTPUT=$(echo "${connectedOutputs}" | grep "${MARQUEE_SCREEN}" | awk 'BEGIN {FS=";"} {print $1}')
    if [[ -n "${MARQUEE_SCREEN}" ]] && [[ -n "${MARQUEE_SCREEN_OUTPUT}" ]]; then
      MARQUEE_SCREEN_CONNECTED="${MARQUEE_SCREEN_OUTPUT}"
    fi
  fi
fi

## List all output position
declare -A position=( ["above"]="--above" \
                      ["below"]="--below" \
                      ["left"]="--left-of" \
                      ["right"]="--right-of"
                    )

# initialize variables
xrandr_cmd="xrandr "

# build xrandr_cmd configuration
## set 2 monitor
if [[ -n ${GAME_SCREEN_CONNECTED} ]] && [[ -n ${MARQUEE_SCREEN_CONNECTED} ]]; then
  ## --primary define Game screen
  ## --above define Marquee Screen above Game Screen
  # xrandr_cmd=${xrandr_cmd}"--output ${GAME_SCREEN_CONNECTED} ${setResolution} --primary --output ${MARQUEE_SCREEN_CONNECTED} ${setMarqueeResolution} --above ${GAME_SCREEN_CONNECTED}"
 xrandr_cmd=${xrandr_cmd}"--output ${GAME_SCREEN_CONNECTED} ${setResolution} --primary --output ${MARQUEE_SCREEN_CONNECTED} ${setMarqueeResolution} ${position[${MARQUEE_SCREEN_POSITION}]} ${GAME_SCREEN_CONNECTED}"

## set only selected game screen
## disable second screen
elif [[ -n ${GAME_SCREEN_CONNECTED} ]] && [[ -z ${MARQUEE_SCREEN_CONNECTED} ]]; then
  xrandr_cmd=${xrandr_cmd}"--output ${GAME_SCREEN_CONNECTED} ${setResolution} ${position[${GAME_SCREEN_POSITION}]}--output ${MARQUEE_SCREEN} --off"

# disable all unedeed monitors and set default if no monitor connected
elif [[ -z ${GAME_SCREEN_CONNECTED} ]] && [[ -z ${MARQUEE_SCREEN_CONNECTED} ]]; then
  xrandr_cmd=${xrandr_cmd}"--output ${connectedOutputs} ${setResolution}"

fi

## check if the xrandr_cmd setup needs to be executed then run it
echo "check recent resolution and maximum resolution for 4k reduce"
echo "$OUTPUT_MAXRES"
echo "Command: $xrandr_cmd"
## Launch cmd 
`$xrandr_cmd`
