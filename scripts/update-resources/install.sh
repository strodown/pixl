#!/bin/bash

#to manage mandatory parameters

helpFunction()
{
   echo "Usage: $0 -e existingVersion -n newVersion -c componentName"
   echo "-e Version of the existing installed version as '0.0.1'"
   echo "-n Version of the existing installed version as '0.0.2'"
   echo "-c name of the component as know by Pegasus"
   exit 1 # Exit script after printing help
}

while getopts ":e:n:c:?" opt; do
   case "$opt" in
      e ) existingVersion=${OPTARG} ;;
      n ) newVersion="$OPTARG" ;;
      c ) componentName="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

#echo for debug
#echo "$existingVersion"
#echo "$newVersion"
#echo "$componentName"

#initialization of progress and state
echo "0.1" > /tmp/$componentName/progress.log
echo "Installation of $componentName..." > /tmp/$componentName/install.log
echo "0" > /tmp/$componentName/install.err

#authorize file system udpate on root
mount -o remount,rw /
mount -o remount,rw /boot

# Print helpFunction in case parameters are empty
if [ -z "$existingVersion" ] || [ -z "$newVersion" ] || [ -z "$componentName" ]
then
   echo "Some or all of the parameters are empty" > /tmp/$componentName/install.log
   echo "1" > /tmp/$componentName/install.err
   helpFunction
fi

#***************************************begin of part to customize in case of OS*****************************************************
if test -n "$(find /tmp/$componentName -maxdepth 1 -name 'recalbox-x86_64.img.xz.sha1' -print -quit)"
then
    echo "0.2" > /tmp/$componentName/progress.log
    #move of files to /boot/update before to reboot
    echo "Move 'recalbox-x86_64.img.xz.sha1' to /boot/update before to reboot" > /tmp/$componentName/install.log
    mv /tmp/$componentName/recalbox-x86_64.img.xz.sha1 /boot/update 
    if [ $? -eq 0 ]
    then
        echo "0.4" > /tmp/$componentName/progress.log
        echo "Move 'recalbox-x86_64.img.xz.sha1' done" > /tmp/$componentName/install.log
    else
        echo "Move 'recalbox-x86_64.img.xz.sha1' failed" > /tmp/$componentName/install.log
        echo "1" > /tmp/$componentName/install.err
        exit $?
    fi
else
    echo "No 'recalbox-x86_64.img.xz.sha1' available in '/tmp/$componentName'" > /tmp/$componentName/install.log
    echo "3" > /tmp/$componentName/install.err
    exit 3
fi
if test -n "$(find /tmp/$componentName -maxdepth 1 -name 'recalbox-x86_64.img.xz' -print -quit)"
then
    echo "0.6" > /tmp/$componentName/progress.log
    #move of files to /boot/update before to reboot
    echo "Move 'recalbox-x86_64.img.xz' to /boot/update before to reboot" > /tmp/$componentName/install.log
    mv /tmp/$componentName/recalbox-x86_64.img.xz /boot/update 
    if [ $? -eq 0 ]
    then
        echo "0.8" > /tmp/$componentName/progress.log
        echo "Move 'recalbox-x86_64.img.xz' done" > /tmp/$componentName/install.log
    else
        echo "Move 'recalbox-x86_64.img.xz' failed" > /tmp/$componentName/install.log
        echo "1" > /tmp/$componentName/install.err
        exit $?
    fi
else
    echo "No 'recalbox-x86_64.img.xz' available in '/tmp/$componentName'" > /tmp/$componentName/install.log
    echo "3" > /tmp/$componentName/install.err
    exit 3
fi
echo "1.0" > /tmp/$componentName/progress.log
echo "Need to reboot to finalize installation" > /tmp/$componentName/install.log
#set 0 if end of installation without other action needed, -1 if need restart of Pegasus, -2 if need reboot
echo "-2" > /tmp/$componentName/install.err
exit 0
#***************************************end of part to customize*****************************************************

