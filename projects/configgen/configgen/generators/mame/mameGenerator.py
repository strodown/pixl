#!/usr/bin/env python
# -*- coding: utf-8 -*-

from configgen.generators.Generator import Generator
import configgen.recalboxFiles as recalboxFiles
from configgen.Command import Command
import shutil
import os
import configparser
from xml.dom import minidom
import codecs
import subprocess


from . import mameControllers

from configgen.Emulator import Emulator
from configgen.controllers.controller import ControllerPerPlayer
from configgen.controllers.inputItem import InputItem
from configgen.settings.keyValueSettings import keyValueSettings


#to use in thefuture for management of bezel for Mame
#import utils.bezels as bezelsUtil

def Log(txt):
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(txt)
    return 0
    

class MameGenerator(Generator):

    def generate(self, system: Emulator, playersControllers: ControllerPerPlayer, recalboxOptions: keyValueSettings, args) -> Command:
    #still guns/resolution to manage
        # Extract "<romfile.zip>"
        romBasename = os.path.basename(os.path.splitext(args.rom)[0])
        romDirname  = os.path.dirname(os.path.splitext(args.rom)[0])

        # Generate "mame" folders if needed
        mamePaths = [ "system/configs/mame", "saves/mame", "saves/mame/nvram", "saves/mame/cfg", "saves/mame/input", "saves/mame/state", "saves/mame/diff", "saves/mame/comments", "bios/mame", "bios/mame/artwork", "cheats/mame", "saves/mame/plugins", "system/configs/mame/ctrlr", "system/configs/mame/ini", "bios/mame/artwork/crosshairs" ]
        for checkPath in mamePaths:
            if not os.path.exists("/recalbox/share/" + checkPath + "/"):
                os.makedirs("/recalbox/share/" + checkPath + "/")



        # MAME options used here are explained as it's not always straightforward
        # A lot more options can be configured, just run mame -showusage and have a look
        commandArray =  [ "/usr/bin/mame/mame" ]
        # or commandArray = [recalboxFiles.recalboxBins[system.Emulator]]
        commandArray += [ "-skip_gameinfo" ]
        commandArray += [ "-rompath",      romDirname ]

        # MAME various paths we can probably do better
        commandArray += [ "-bgfx_path",    "/usr/bin/mame/bgfx/" ]          # Core bgfx files can be left on ROM filesystem
        commandArray += [ "-fontpath",     "/usr/bin/mame/" ]               # Fonts can be left on ROM filesystem
        commandArray += [ "-languagepath", "/usr/bin/mame/language/" ]      # Translations can be left on ROM filesystem
        commandArray += [ "-pluginspath", "/usr/bin/mame/plugins/;/recalbox/share/saves/mame/plugins" ]
        commandArray += [ "-samplepath",   "/recalbox/share/bios/mame/samples/" ] # storage location for MAME samples : TBC for pixL case
        commandArray += [ "-artpath",       "/recalbox/share/decorations/;/var/run/mame_artwork/;/usr/bin/mame/artwork/" ] # first for systems ; second for overlays

        # Enable cheats
        commandArray += [ "-cheat" ]
        commandArray += [ "-cheatpath",    "/recalbox/share/cheats/mame/" ]       # Should this point to path containing the cheat.7z file
        # logs
        # TO DO: need parameter to add condition
        commandArray += [ "-verbose" ]
        # MAME saves a lot of stuff, we need to map this on /recalbox/share/saves/mame/<subfolder> for each one
        commandArray += [ "-nvram_directory" ,    "/recalbox/share/saves/mame/nvram/" ]
        
        #TO DO: no custom conf management for the moment
        customCfg = False
        # No special controller management
        specialController = "none"
        
        cfgPath = "/recalbox/share/system/configs/mame/"
        if not os.path.exists("/recalbox/share/system/configs/mame/"):
            os.makedirs("/recalbox/share/system/configs/mame/")
        commandArray += [ "-cfg_directory"   ,    cfgPath ]
        
        commandArray += [ "-input_directory" ,    "/recalbox/share/saves/mame/input/" ]
        commandArray += [ "-state_directory" ,    "/recalbox/share/saves/mame/state/" ]
        commandArray += [ "-snapshot_directory" , "/recalbox/share/screenshots/" ]
        commandArray += [ "-diff_directory" ,     "/recalbox/share/saves/mame/diff/" ]
        commandArray += [ "-comment_directory",   "/recalbox/share/saves/mame/comments/" ]
        commandArray += [ "-homepath" ,           "/recalbox/share/saves/mame/plugins/" ]

        ctrlrPath = "/recalbox/share/system/configs/mame/ctrlr/"
        if not os.path.exists("/recalbox/share/system/configs/mame/ctrlr/"):
            os.makedirs("/recalbox/share/system/configs/mame/ctrlr/")
        commandArray += [ "-ctrlrpath" ,          "/recalbox/share/system/configs/mame/ctrlr/" ]
        commandArray += [ "-ctrlr" , "default" ]

        commandArray += [ "-inipath" ,            "/recalbox/share/system/configs/mame/;/recalbox/share/system/configs/mame/ini/" ]
        commandArray += [ "-crosshairpath" ,      "/recalbox/share/bios/mame/artwork/crosshairs/" ]

        # Other video modes
        # TO DO: need parameter to add condition
        #commandArray += ["-video", "accel" ]
        # or
        commandArray += [ "-video", "opengl" ]

        # TO DO: need parameter to add condition to manage resolution
        #commandArray += [ "-resolution", "{}x{}".format(gameResolution["width"], gameResolution["height"]) ]
        # Refresh rate options to help with screen tearing
        # syncrefresh is unlisted, it requires specific display timings and 99.9% of users will get unplayable games.
        # Leaving it so it can be set manually, for CRT or other arcade-specific display users.
        # TO DO: need parameter to activate or not
        commandArray += [ "-waitvsync" ]
        # TO DO: need parameter to activate or not
        commandArray += [ "-syncrefresh" ]


        

        # Load selected plugins
        #TO DO : manage plugins if needed
        #pluginsToLoad = []
        #if not (system.isOptSet("hiscoreplugin") and system.getOptBoolean("hiscoreplugin") == False):
        #    pluginsToLoad += [ "hiscore" ]
        #if system.isOptSet("coindropplugin") and system.getOptBoolean("coindropplugin"):
        #    pluginsToLoad += [ "coindrop" ]
        #if system.isOptSet("dataplugin") and system.getOptBoolean("dataplugin"):
        #    pluginsToLoad += [ "data" ]
        #if len(pluginsToLoad) > 0:
        #    commandArray += [ "-plugins", "-plugin", ",".join(pluginsToLoad) ]
        # Mouse
        #TO DO : manage mouse/multimouse/gun if needed
        useMouse = False
        #if (system.isOptSet('use_mouse') and system.getOptBoolean('use_mouse')) or not (messSysName[messMode] == "" or messMode == -1):
        #    useMouse = True
        #    commandArray += [ "-dial_device", "mouse" ]
        #    commandArray += [ "-trackball_device", "mouse" ]
        #    commandArray += [ "-paddle_device", "mouse" ]
        #    commandArray += [ "-positional_device", "mouse" ]
        #    commandArray += [ "-mouse_device", "mouse" ]
        #    commandArray += [ "-ui_mouse" ]
        #    if not (system.isOptSet('use_guns') and system.getOptBoolean('use_guns')):
        #        commandArray += [ "-lightgun_device", "mouse" ]
        #        commandArray += [ "-adstick_device", "mouse" ]
        #else:
        commandArray += [ "-dial_device", "joystick" ]
        commandArray += [ "-trackball_device", "joystick" ]
        commandArray += [ "-paddle_device", "joystick" ]
        commandArray += [ "-positional_device", "joystick" ]
        commandArray += [ "-mouse_device", "joystick" ]
        #if not (system.isOptSet('use_guns') and system.getOptBoolean('use_guns')):
        commandArray += [ "-lightgun_device", "joystick" ]
        commandArray += [ "-adstick_device", "joystick" ]
        # Multimouse option currently hidden in ES, SDL only detects one mouse.
        # Leaving code intact for testing & possible ManyMouse integration
        multiMouse = False
        #if system.isOptSet('multimouse') and system.getOptBoolean('multimouse'):
        #    multiMouse = True
        #    commandArray += [ "-multimouse" ]

        # guns
        useGuns = False
        #if system.isOptSet('use_guns') and system.getOptBoolean('use_guns'):
        #    useGuns = True
        #    commandArray += [ "-lightgunprovider", "udev" ]
        #    commandArray += [ "-lightgun_device", "lightgun" ]
        #    commandArray += [ "-adstick_device", "lightgun" ]
        #if system.isOptSet('offscreenreload') and system.getOptBoolean('offscreenreload'):
        #    commandArray += [ "-offscreen_reload" ]
        # Finally we pass game name
        commandArray += [ romBasename ]

        # config file
        #config = minidom.Document()
        #configFile = "/recalbox/share/system/configs/mame/default.cfg"
        #if os.path.exists(configFile):
            #try:
                #config = minidom.parse(configFile)
            #except:
        #        pass # reinit the file

        # save the config file
        #mameXml = open(configFile, "w")
        # TODO: python 3 - workawround to encode files in utf-8
        #mameXml = codecs.open(configFile, "w", "utf-8")
        #dom_string = os.linesep.join([s for s in config.toprettyxml().splitlines() if s.strip()]) # remove ugly empty lines while minicom adds them...
        #mameXml.write(dom_string)

        # bezels
        #TO DO: to manage bezels
        #if 'bezel' not in system.config.keys() or system.config['bezel'] == '':
        bezelSet = None
        #else:
        #    bezelSet = system.config['bezel']
        #if system.isOptSet('forceNoBezel') and system.getOptBoolean('forceNoBezel'):
        #    bezelSet = None
        #try:
        #    if messMode != -1:
        #        MameGenerator.writeBezelConfig(bezelSet, system, rom, messSysName[messMode], gameResolution, controllersConfig.gunsBordersSizeName(guns, system.config))
        #    else:
        #        MameGenerator.writeBezelConfig(bezelSet, system, rom, "", gameResolution, controllersConfig.gunsBordersSizeName(guns, system.config))
        #except:
        #    MameGenerator.writeBezelConfig(None, system, rom, "", gameResolution, controllersConfig.gunsBordersSizeName(guns, system.config))
        #buttonLayout = getMameControlScheme(system, romBasename)
        buttonLayout = "default"
        
        mameControllers.generatePadsConfig(ctrlrPath, playersControllers, "", buttonLayout, customCfg, specialController, bezelSet, useGuns, useMouse, multiMouse)
        
        # Change directory to MAME folder (allows data plugin to load properly)
        os.chdir('/usr/bin/mame')
        return Command(videomode=system.VideoMode, array=commandArray, env={"XDG_CONFIG_HOME":recalboxFiles.CONF, "XDG_DATA_HOME":recalboxFiles.SAVES})
