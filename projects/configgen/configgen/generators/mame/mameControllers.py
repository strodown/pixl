#!/usr/bin/env python
# -*- coding: utf-8 -*-


import configgen.recalboxFiles as recalboxFiles
from configgen.Command import Command
from configgen.controllers.inputItem import InputItem
import shutil
import os
import configparser
from xml.dom import minidom
import codecs
import subprocess

#to use in thefuture for management of bezel for Mame
#import utils.bezels as bezelsUtil

#for future ?!
#import csv
#from xml.dom import minidom
#from PIL import Image, ImageOps

def Log(txt):
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(txt)
    return 0
    

def generatePadsConfig(cfgPath, playersControllers, sysName, altButtons, customCfg, specialController, decorations, useGuns, useMouse, multiMouse):

    # config file
    config = minidom.Document()
    configFile = cfgPath + "default.cfg"
    if os.path.exists(configFile):
        try:
            config = minidom.parse(configFile)
        except:
            pass # reinit the file
    if os.path.exists(configFile) and customCfg:
        overwriteMAME = False
    else:
        overwriteMAME = True
    
    # # Load standard controls from csv
    # controlFile = '/usr/lib/python3.9/site-packages/configgen/data/mame/mameControls.csv'
    # openFile = open(controlFile, 'r')
    # controlDict = {}
    # with openFile:
        # controlList = csv.reader(openFile)
        # for row in controlList:
            # if not row[0] in controlDict.keys():
                # controlDict[row[0]] = {}
            # controlDict[row[0]][row[1]] = row[2]

    # # Common controls
    mappings = {
        "JOYSTICK_UP":    "joystick1up",
        "JOYSTICK_DOWN":  "joystick1down",
        "JOYSTICK_LEFT":  "joystick1left",
        "JOYSTICK_RIGHT": "joystick1right",
        "JOYSTICKLEFT_UP":    "joystick2up",
        "JOYSTICKLEFT_DOWN":  "joystick2down",
        "JOYSTICKLEFT_LEFT":  "joystick2left",
        "JOYSTICKLEFT_RIGHT": "joystick2right",
        "BUTTON1": "b",
        "BUTTON2": "y",
        "BUTTON3": "a",
        "BUTTON4": "x",
        "BUTTON5": "pageup",
        "BUTTON6": "pagedown",
        "BUTTON7": "l2",
        "BUTTON8": "r2",
        "BUTTON9": "l3",
        "BUTTON10": "r3",
        "START": "start",
        "COIN": "select"
    }
    # for controlDef in controlDict['default'].keys():
        # mappings[controlDef] = controlDict['default'][controlDef]

    # # Only use gun buttons if lightguns are enabled to prevent conflicts with mouse
    gunmappings = {}
    # if useGuns:
        # for controlDef in controlDict['gunbuttons'].keys():
            # gunmappings[controlDef] = controlDict['gunbuttons'][controlDef]

    # # Only define mouse buttons if mouse is enabled, to prevent unwanted inputs
    # # For a standard mouse, left, right, scroll wheel should be mapped to action buttons, and if side buttons are available, they will be coin & start
    mousemappings = {}
    # if useMouse:
        # for controlDef in controlDict['mousebuttons'].keys():
            # mousemappings[controlDef] = controlDict['mousebuttons'][controlDef]

    # # Buttons that change based on game/setting
    # if altButtons != "":
        # for controlDef in controlDict[altButtons].keys():
            # mappings.update({controlDef: controlDict[altButtons][controlDef]})

    xml_mameconfig = getRoot(config, "mameconfig")
    xml_mameconfig.setAttribute("version", "10") # otherwise, config of pad won't work at first run
    xml_system     = getSection(config, xml_mameconfig, "system")
    xml_system.setAttribute("name", "default")

    removeSection(config, xml_system, "input")
    
    xml_input = config.createElement("input")
    xml_system.appendChild(xml_input)

    # Fill in controls on cfg files
    nplayer = 1
    maxplayers = len(playersControllers)
    for playercontroller, pad in sorted(playersControllers.items()):
        #map device to avoid bad affectation of player vs pad
        xml_mapdevice = config.createElement("mapdevice")
        #Log("pad.DeviceName : {}".format(pad.DeviceName))
        splitted = pad.DeviceName.split(" - ")
        #Log("pad.DeviceName.split(' - ')[0] : {}".format(splitted[0]))
        xml_mapdevice.setAttribute("device", splitted[0])
        #Log("pad.playerIndex : {}".format(pad.PlayerIndex))
        #Log("pad.SdlIndex : {}".format(pad.SdlIndex))
        #Log("pad.NaturalIndex : {}".format(pad.NaturalIndex))
        indexToUse = pad.SdlIndex + 1
        xml_mapdevice.setAttribute("controller", "JOYCODE_{}".format(indexToUse))
        xml_input.appendChild(xml_mapdevice)
        
        #see if no stick are available
        mappings_use = mappings
        if not pad.HasInput(InputItem.ConverterNameToItem["joystick1up"]):
            mappings_use["JOYSTICK_UP"] = "up"
            mappings_use["JOYSTICK_DOWN"] = "down"
            mappings_use["JOYSTICK_LEFT"] = "left"
            mappings_use["JOYSTICK_RIGHT"] = "right"
        else: #need to fix to restore initial value because recopy doesn't work
            mappings_use["JOYSTICK_UP"] = "joystick1up"
            mappings_use["JOYSTICK_DOWN"] = "joystick1down"
            mappings_use["JOYSTICK_LEFT"] = "joystick1left"
            mappings_use["JOYSTICK_RIGHT"] = "joystick1right"

        #for input in pad.AvailableInput:
            # Log("input.Name : {}".format(input.Name))
            # Log("input.Type : {}".format(InputItem.ConverterTypeToName[input.Type]))
            # Log("input.Type(int) : {}".format(input.Type))
            # Log("input.Value : {}".format(input.Value))

        #for later and for guns
        #addCommonPlayerPorts(config, xml_input, nplayer)

        for mapping in mappings_use:
            if pad.HasInput(InputItem.ConverterNameToItem[mappings_use[mapping]]):
                # Log("mappings_use[{}]: {}".format(mapping,mappings_use[mapping]))
                if mapping in [ 'START', 'COIN' ]:
                    xml_input.appendChild(generateSpecialPortElementPlayer(pad, config, 'standard', nplayer, indexToUse, mapping, mappings_use[mapping], pad.Input(InputItem.ConverterNameToItem[mappings_use[mapping]]), False, "", "", gunmappings, mousemappings, multiMouse))
                else:
                    xml_input.appendChild(generatePortElement(pad, config, nplayer, indexToUse, mapping, mappings_use[mapping], pad.Input(InputItem.ConverterNameToItem[mappings_use[mapping]]), False, altButtons, gunmappings, mousemappings, multiMouse))
            else:
                rmapping = reverseMapping(mappings_use[mapping])
                # Log("rmapping: {}".format(rmapping))
                if not rmapping == None:
                    if pad.HasInput(InputItem.ConverterNameToItem[rmapping]):
                        #Log("pad.HasInput({})".format(rmapping))
                        xml_input.appendChild(generatePortElement(pad, config, nplayer, indexToUse, mapping, mappings_use[mapping], pad.Input(InputItem.ConverterNameToItem[rmapping]), True, altButtons, gunmappings, mousemappings, multiMouse))

        #UI Mappings
        if nplayer == 1:
            xml_input.appendChild(generateComboPortElement(pad, config, 'standard', indexToUse, "UI_DOWN", "DOWN", mappings_use["JOYSTICK_DOWN"], pad.Input(InputItem.ConverterNameToItem[mappings_use["JOYSTICK_UP"]]), False, "", ""))      # Down
            xml_input.appendChild(generateComboPortElement(pad, config, 'standard', indexToUse, "UI_LEFT", "LEFT", mappings_use["JOYSTICK_LEFT"], pad.Input(InputItem.ConverterNameToItem[mappings_use["JOYSTICK_LEFT"]]), False, "", ""))    # Left
            xml_input.appendChild(generateComboPortElement(pad, config, 'standard', indexToUse, "UI_UP", "UP", mappings_use["JOYSTICK_UP"], pad.Input(InputItem.ConverterNameToItem[mappings_use["JOYSTICK_UP"]]), False, "", ""))            # Up
            xml_input.appendChild(generateComboPortElement(pad, config, 'standard', indexToUse, "UI_RIGHT", "RIGHT", mappings_use["JOYSTICK_RIGHT"], pad.Input(InputItem.ConverterNameToItem[mappings_use["JOYSTICK_LEFT"]]), False, "", "")) # Right
            xml_input.appendChild(generateComboPortElement(pad, config, 'standard', indexToUse, "UI_SELECT", "ENTER", 'b', pad.Input(InputItem.ConverterNameToItem['b']), False, "", ""))                                                     # Select

        # if useControls in messControlDict.keys():
            # for controlDef in messControlDict[useControls].keys():
                # thisControl = messControlDict[useControls][controlDef]
                # if nplayer == thisControl['player']:
                    # if thisControl['type'] == 'special':
                        # xml_input_alt.appendChild(generateSpecialPortElement(pad, config_alt, thisControl['tag'], nplayer, pad.index, thisControl['key'], thisControl['mapping'], \
                            # pad.inputs[mappings_use[thisControl['useMapping']]], thisControl['reversed'], thisControl['mask'], thisControl['default']))
                    # elif thisControl['type'] == 'main':
                        # xml_input.appendChild(generateSpecialPortElement(pad, config_alt, thisControl['tag'], nplayer, pad.index, thisControl['key'], thisControl['mapping'], \
                            # pad.inputs[mappings_use[thisControl['useMapping']]], thisControl['reversed'], thisControl['mask'], thisControl['default']))
                    # elif thisControl['type'] == 'analog':
                        # xml_input_alt.appendChild(generateAnalogPortElement(pad, config_alt, thisControl['tag'], nplayer, pad.index, thisControl['key'], mappings_use[thisControl['incMapping']], \
                            # mappings_use[thisControl['decMapping']], pad.inputs[mappings_use[thisControl['useMapping1']]], pad.inputs[mappings_use[thisControl['useMapping2']]], thisControl['reversed'], \
                            # thisControl['mask'], thisControl['default'], thisControl['delta'], thisControl['axis']))
                    # elif thisControl['type'] == 'combo':
                        # xml_input_alt.appendChild(generateComboPortElement(pad, config_alt, thisControl['tag'], pad.index, thisControl['key'], thisControl['kbMapping'], thisControl['mapping'], \
                            # pad.inputs[mappings_use[thisControl['useMapping']]], thisControl['reversed'], thisControl['mask'], thisControl['default']))

        nplayer = nplayer + 1

    # save the config file
    #mameXml = open(configFile, "w")
    # TODO: python 3 - workawround to encode files in utf-8
    if overwriteMAME:
        Log(f"Saving {configFile}")
        mameXml = codecs.open(configFile, "w", "utf-8")
        dom_string = os.linesep.join([s for s in config.toprettyxml().splitlines() if s.strip()]) # remove ugly empty lines while minicom adds them...
        mameXml.write(dom_string)

    # # Write alt config (if used, custom config is turned off or file doesn't exist yet)
    # if sysName in specialControlList and overwriteSystem:
        # eslog.debug(f"Saving {configFile_alt}")
        # mameXml_alt = codecs.open(configFile_alt, "w", "utf-8")
        # dom_string_alt = os.linesep.join([s for s in config_alt.toprettyxml().splitlines() if s.strip()]) # remove ugly empty lines while minicom adds them...
        # mameXml_alt.write(dom_string_alt)

def reverseMapping(key):
    if key == "joystick1down":
        return "joystick1up"
    if key == "joystick1right":
        return "joystick1left"
    if key == "joystick2down":
        return "joystick2up"
    if key == "joystick2right":
        return "joystick2left"
    return None

def generatePortElement(pad, config, nplayer, padindex, mapping, key, input, reversed, altButtons, gunmappings, mousemappings, multiMouse):
    # Generic input
    xml_port = config.createElement("port")
    xml_port.setAttribute("type", "P{}_{}".format(nplayer, mapping))
    xml_newseq = config.createElement("newseq")
    xml_newseq.setAttribute("type", "standard")
    xml_port.appendChild(xml_newseq)
    keyval = input2definition(pad, key, input, padindex, reversed, altButtons)
    if mapping in gunmappings:
        keyval = keyval + " OR GUNCODE_{}_{}".format(nplayer, gunmappings[mapping])
    if mapping in mousemappings:
        if multiMouse:
            keyval = keyval + " OR MOUSECODE_{}_{}".format(nplayer, mousemappings[mapping])
        else:
            keyval = keyval + " OR MOUSECODE_1_{}".format(mousemappings[mapping])
    value = config.createTextNode(keyval)
    xml_newseq.appendChild(value)
    return xml_port

def generateSpecialPortElementPlayer(pad, config, tag, nplayer, padindex, mapping, key, input, reversed, mask, default, gunmappings, mousemappings, multiMouse):
    # Special button input (ie mouse button to gamepad)
    xml_port = config.createElement("port")
    xml_port.setAttribute("tag", tag)
    xml_port.setAttribute("type", mapping+str(nplayer))
    xml_port.setAttribute("mask", mask)
    xml_port.setAttribute("defvalue", default)
    xml_newseq = config.createElement("newseq")
    xml_newseq.setAttribute("type", "standard")
    xml_port.appendChild(xml_newseq)
    keyval = input2definition(pad, key, input, padindex, reversed, 0)
    if mapping in gunmappings:
        keyval = keyval + " OR GUNCODE_{}_{}".format(nplayer, gunmappings[mapping])
    if mapping in mousemappings:
        if multiMouse:
            keyval = keyval + " OR MOUSECODE_{}_{}".format(nplayer, mousemappings[mapping])
        else:
            keyval = keyval + " OR MOUSECODE_1_{}".format(mousemappings[mapping])
    value = config.createTextNode(keyval)
    xml_newseq.appendChild(value)
    return xml_port

def generateSpecialPortElement(pad, config, tag, nplayer, padindex, mapping, key, input, reversed, mask, default):
    # Special button input (ie mouse button to gamepad)
    xml_port = config.createElement("port")
    xml_port.setAttribute("tag", tag)
    xml_port.setAttribute("type", mapping)
    xml_port.setAttribute("mask", mask)
    xml_port.setAttribute("defvalue", default)
    xml_newseq = config.createElement("newseq")
    xml_newseq.setAttribute("type", "standard")
    xml_port.appendChild(xml_newseq)
    value = config.createTextNode(input2definition(pad, key, input, padindex, reversed, 0))
    xml_newseq.appendChild(value)
    return xml_port

def generateComboPortElement(pad, config, tag, padindex, mapping, kbkey, key, input, reversed, mask, default):
    # Maps a keycode + button - for important keyboard keys when available
    xml_port = config.createElement("port")
    xml_port.setAttribute("tag", tag)
    xml_port.setAttribute("type", mapping)
    xml_port.setAttribute("mask", mask)
    xml_port.setAttribute("defvalue", default)
    xml_newseq = config.createElement("newseq")
    xml_newseq.setAttribute("type", "standard")
    xml_port.appendChild(xml_newseq)
    value = config.createTextNode("KEYCODE_{} OR ".format(kbkey) + input2definition(pad, key, input, padindex, reversed, 0))
    xml_newseq.appendChild(value)
    return xml_port

def generateAnalogPortElement(pad, config, tag, nplayer, padindex, mapping, inckey, deckey, mappedinput, mappedinput2, reversed, mask, default, delta, axis = ''):
    # Mapping analog to digital (mouse, etc)
    xml_port = config.createElement("port")
    xml_port.setAttribute("tag", tag)
    xml_port.setAttribute("type", mapping)
    xml_port.setAttribute("mask", mask)
    xml_port.setAttribute("defvalue", default)
    xml_port.setAttribute("keydelta", delta)
    xml_newseq_inc = config.createElement("newseq")
    xml_newseq_inc.setAttribute("type", "increment")
    xml_port.appendChild(xml_newseq_inc)
    incvalue = config.createTextNode(input2definition(pad, inckey, mappedinput, padindex, reversed, 0, True))
    xml_newseq_inc.appendChild(incvalue)
    xml_newseq_dec = config.createElement("newseq")
    xml_port.appendChild(xml_newseq_dec)
    xml_newseq_dec.setAttribute("type", "decrement")
    decvalue = config.createTextNode(input2definition(pad, deckey, mappedinput2, padindex, reversed, 0, True))
    xml_newseq_dec.appendChild(decvalue)
    xml_newseq_std = config.createElement("newseq")
    xml_port.appendChild(xml_newseq_std)
    xml_newseq_std.setAttribute("type", "standard")
    if axis == '':
        stdvalue = config.createTextNode("NONE")
    else:
        stdvalue = config.createTextNode("JOYCODE_{}_{}".format(padindex, axis))
    xml_newseq_std.appendChild(stdvalue)
    return xml_port

def input2definition(pad, key, input, joycode, reversed, altButtons, ignoreAxis = False):
    # Log("input.Name : {}".format(input.Name))
    # Log("input.Type : {}".format(InputItem.ConverterTypeToName[input.Type]))
    # Log("input.Type(int) : {}".format(input.Type))
    # Log("InputItem.TypeHat(int) : {}".format(InputItem.TypeHat))
    # Log("input.Value : {}".format(input.Value))
    if input.Type == InputItem.TypeButton:
        return f"JOYCODE_{joycode}_BUTTON{int(input.Id)+1}"
    elif input.Type == InputItem.TypeHat:
        # Log("input.Type == InputItem.TypeHat")
        if input.Value == 1:
            return f"JOYCODE_{joycode}_HAT1UP"
        elif input.Value == 2:
            return f"JOYCODE_{joycode}_HAT1RIGHT"
        elif input.Value == 4:
            return f"JOYCODE_{joycode}_HAT1DOWN"
        elif input.Value == 8:
            return f"JOYCODE_{joycode}_HAT1LEFT"
    elif input.Type == InputItem.TypeAxis:
        # Determine alternate button for D-Pad and right stick as buttons
        dpadInputs = {}
        for direction in ['up', 'down', 'left', 'right']:
            if pad.Input(InputItem.ConverterNameToItem[direction]).Type == InputItem.TypeButton:
                dpadInputs[direction] = f'JOYCODE_{joycode}_BUTTON{int(pad.Input(InputItem.ConverterNameToItem[direction]).Id)+1}'
            elif pad.Input(InputItem.ConverterNameToItem[direction]).Type == InputItem.TypeHat:
                if pad.Input(InputItem.ConverterNameToItem[direction]).Value == 1:
                    dpadInputs[direction] = f'JOYCODE_{joycode}_HAT1UP'
                if pad.Input(InputItem.ConverterNameToItem[direction]).Value == 2:
                    dpadInputs[direction] = f'JOYCODE_{joycode}_HAT1RIGHT'
                if pad.Input(InputItem.ConverterNameToItem[direction]).Value == 4:
                    dpadInputs[direction] = f'JOYCODE_{joycode}_HAT1DOWN'
                if pad.Input(InputItem.ConverterNameToItem[direction]).Value == 8:
                    dpadInputs[direction] = f'JOYCODE_{joycode}_HAT1LEFT'
            else:
                dpadInputs[direction] = ''
        buttonDirections = {}
        # workarounds for issue #6892
        # Modified because right stick to buttons was not working after the workaround
        # Creates a blank, only modifies if the button exists in the pad.
        # Button assigment modified - blank "OR" gets removed by MAME if the button is undefined.
        for direction in ['a', 'b', 'x', 'y']:
            buttonDirections[direction] = ''
            if pad.HasInput(InputItem.ConverterNameToItem[direction]):
                if pad.Input(InputItem.ConverterNameToItem[direction]).Type == InputItem.TypeButton:
                    buttonDirections[direction] = f'JOYCODE_{joycode}_BUTTON{int(pad.Input(InputItem.ConverterNameToItem[direction]).Id)+1}'

        if ignoreAxis and dpadInputs['up'] != '' and dpadInputs['down'] != '' \
            and dpadInputs['left'] != '' and dpadInputs['right'] != '':
            if key == "joystick1up" or key == "up":
                return dpadInputs['up']
            if key == "joystick1down" or key == "down":
                return dpadInputs['down']
            if key == "joystick1left" or key == "left":
                return dpadInputs['left']
            if key == "joystick1right" or key == "right":
                return dpadInputs['right']
        if altButtons == "qbert": # Q*Bert Joystick
            if key == "joystick1up" or key == "up":
                return f"JOYCODE_{joycode}_YAXIS_UP_SWITCH JOYCODE_{joycode}_XAXIS_RIGHT_SWITCH OR {dpadInputs['up']} {dpadInputs['right']}"
            if key == "joystick1down" or key == "down":
                return f"JOYCODE_{joycode}_YAXIS_DOWN_SWITCH JOYCODE_{joycode}_XAXIS_LEFT_SWITCH OR {dpadInputs['down']} {dpadInputs['left']}"
            if key == "joystick1left" or key == "left":
                return f"JOYCODE_{joycode}_XAXIS_LEFT_SWITCH JOYCODE_{joycode}_YAXIS_UP_SWITCH OR {dpadInputs['left']} {dpadInputs['up']}"
            if key == "joystick1right" or key == "right":
                return f"JOYCODE_{joycode}_XAXIS_RIGHT_SWITCH JOYCODE_{joycode}_YAXIS_DOWN_SWITCH OR {dpadInputs['right']} {dpadInputs['down']}"
        else:
            if key == "joystick1up" or key == "up":
                #to manage case of stick wityh X axe swapped with Y axe
                if key == "up" and input.Id == 0:
                    # up -> left for AXIS part only
                    return f"JOYCODE_{joycode}_XAXIS_LEFT_SWITCH OR {dpadInputs['up']}"
                return f"JOYCODE_{joycode}_YAXIS_UP_SWITCH OR {dpadInputs['up']}"
            if key == "joystick1down" or key == "down":
                #to manage case of stick wityh X axe swapped with Y axe
                if key == "down" and input.Id == 0:
                    # down -> right for AXIS part only
                    return f"JOYCODE_{joycode}_XAXIS_RIGHT_SWITCH OR {dpadInputs['down']}"
                return f"JOYCODE_{joycode}_YAXIS_DOWN_SWITCH OR {dpadInputs['down']}"
            if key == "joystick1left" or key == "left":
                #to manage case of stick wityh X axe swapped with Y axe
                if key == "left" and input.Id == 1:
                    # left -> down for AXIS part only
                    return f"JOYCODE_{joycode}_YAXIS_DOWN_SWITCH OR {dpadInputs['left']}"
                return f"JOYCODE_{joycode}_XAXIS_LEFT_SWITCH OR {dpadInputs['left']}"
            if key == "joystick1right" or key == "right":
                #to manage case of stick wityh X axe swapped with Y axe
                if key == "right" and input.Id == 1:
                    # right -> up for AXIS part only
                    return f"JOYCODE_{joycode}_YAXIS_UP_SWITCH OR {dpadInputs['right']}"
                return f"JOYCODE_{joycode}_XAXIS_RIGHT_SWITCH OR {dpadInputs['right']}"
        # Fix for the workaround
        for direction in pad.AvailableInput:
            if(key == "joystick2up"):
                return f"JOYCODE_{joycode}_RYAXIS_NEG_SWITCH OR {buttonDirections['x']}"
            if(key == "joystick2down"):
                return f"JOYCODE_{joycode}_RYAXIS_POS_SWITCH OR {buttonDirections['b']}"
            if(key == "joystick2left"):
                return f"JOYCODE_{joycode}_RXAXIS_NEG_SWITCH OR {buttonDirections['y']}"
            if(key == "joystick2right"):
                return f"JOYCODE_{joycode}_RXAXIS_POS_SWITCH OR {buttonDirections['a']}"
            if int(input.Id) == 2: # XInput L2
                return f"JOYCODE_{joycode}_ZAXIS_POS_SWITCH"
            if int(input.Id) == 5: # XInput R2
                return f"JOYCODE_{joycode}_RZAXIS_POS_SWITCH"
    return "unknown"

def hasStick(pad):
    if pad.HasInput(InputItem.ConverterNameToItem["joystick1up"]):
        return True
    else:
        return False

def getRoot(config, name):
    xml_section = config.getElementsByTagName(name)

    if len(xml_section) == 0:
        xml_section = config.createElement(name)
        config.appendChild(xml_section)
    else:
        xml_section = xml_section[0]

    return xml_section

def getSection(config, xml_root, name):
    xml_section = xml_root.getElementsByTagName(name)

    if len(xml_section) == 0:
        xml_section = config.createElement(name)
        xml_root.appendChild(xml_section)
    else:
        xml_section = xml_section[0]

    return xml_section

def removeSection(config, xml_root, name):
    xml_section = xml_root.getElementsByTagName(name)

    for i in range(0, len(xml_section)):
        old = xml_root.removeChild(xml_section[i])
        old.unlink()

def addCommonPlayerPorts(config, xml_input, nplayer):
    # adstick for guns
    for axis in ["X", "Y"]:
        nanalog = 1 if axis == "X" else 2
        xml_port = config.createElement("port")
        xml_port.setAttribute("tag", ":mainpcb:ANALOG{}".format(nanalog))
        xml_port.setAttribute("type", "P{}_AD_STICK_{}".format(nplayer, axis))
        xml_port.setAttribute("mask", "255")
        xml_port.setAttribute("defvalue", "128")
        xml_newseq = config.createElement("newseq")
        xml_newseq.setAttribute("type", "standard")
        xml_port.appendChild(xml_newseq)
        value = config.createTextNode("GUNCODE_{}_{}AXIS".format(nplayer, axis))
        xml_newseq.appendChild(value)
        xml_input.appendChild(xml_port)
