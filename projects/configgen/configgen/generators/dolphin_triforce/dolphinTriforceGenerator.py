#!/usr/bin/env python
from configgen.Command import Command
import configgen.recalboxFiles as recalboxFiles
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.Emulator import Emulator
from configgen.generators.Generator import Generator
import shutil
import os.path
from os import environ
import configparser
from . import dolphinTriforceControllers
import subprocess

def Log(txt):
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(txt)
    return 0

class DolphinTriforceGenerator(Generator):

    # Game Ratio
    GAME_RATIO = \
    {
       "auto": 0,
       "16/9": 1, 
       "4/3": 2,
       "squarepixel": 3,
    }

    # Gamecube languages
    GAMECUBE_LANGUAGES = \
    {
        "en": 0,
        "de": 1,
        "fr": 2,
        "es": 3,
        "it": 4,
        "du": 5,
    }

    # Triforce Game need slot 0 save state to be launch
    GAME_NEED_SAVESATE_FIRST = \
    {
        "GGPE01",
        "GFZJ8P",
    }

    # Triforce Game need to remove card created to avoid issue in game
    GAME_NEED_CLEANCARD_FIRST = \
    {
        "GGPE01",
    }
    
    # Setting on dolphin.ini
    SECTION_GENERAL = "General"
    SECTION_CORE = "Core"
    SECTION_INTERFACE = "Interface"
    SECTION_ANALYTICS = "Analytics"
    SECTION_DISPLAY = "Display"
    # Setting on gfx_opengl.ini
    SECTION_SETTINGS = "Settings"
    SECTION_HACKS = "Hacks"
    SECTION_ENHANCEMENTS = "Enhancements"
    SECTION_HARDWARE = "Hardware"
    # Setting on Logger.ini
    SECTION_LOGS = "Logs"

    # Get keyboard layout
    @staticmethod
    def GetLanguage() -> str:
        conf = keyValueSettings(recalboxFiles.recalboxConf)
        conf.loadFile(True)
        # Try to obtain from keyboard layout, then from system language, then fallback to us
        kl = conf.getString("system.kblayout", conf.getString("system.language", "en")[0:2]).lower()
        return kl

    def generate(self, system: Emulator, playersControllers, gameResolution, args):
        if not os.path.exists(os.path.dirname(recalboxFiles.dolphinTriforceIni)):
            os.makedirs(os.path.dirname(recalboxFiles.dolphinTriforceIni))

        language = self.GetLanguage()
        gamecubeLanguage = self.GAMECUBE_LANGUAGES[language] if language in self.GAMECUBE_LANGUAGES else 0

        # Dir required for saves
        if not os.path.exists(recalboxFiles.dolphinTriforceData + "/StateSaves"):
            os.makedirs(recalboxFiles.dolphinTriforceData + "/StateSaves")

        dolphinTriforceControllers.generateControllerConfig(system, playersControllers, args.rom)

        # Setting on dolphin.ini
        # Load Configuration
        from configgen.settings.iniSettings import IniSettings
        iniSettings = IniSettings(recalboxFiles.dolphinTriforceIni, True)
        iniSettings.loadFile(True)

        # Define default games path
        iniSettings.setString(self.SECTION_GENERAL, "ISOPath0", recalboxFiles.ROMS + "/triforce")
        iniSettings.setString(self.SECTION_GENERAL, "ISOPaths", "1")
        iniSettings.setString(self.SECTION_GENERAL, "GCMPath0", recalboxFiles.ROMS + "/triforce")
        iniSettings.setString(self.SECTION_GENERAL, "GCMPathes", "1")
        # # Draw or not FPS
        iniSettings.setBool(self.SECTION_GENERAL, "ShowFrameCount", system.ShowFPS) # system.ShowFPS
        # Save file location
        iniSettings.setString(self.SECTION_CORE, "MemcardAPath", recalboxFiles.SAVES + "/dolphin-triforce/GC/MemoryCardA.USA.raw")
        iniSettings.setString(self.SECTION_CORE, "MemcardBPath", recalboxFiles.SAVES + "/dolphin-triforce/GC/MemoryCardB.USA.raw")
        # Don't ask about statistics
        iniSettings.setString(self.SECTION_ANALYTICS, "PermissionAsked", "True")
        # PanicHandlers displaymessages
        iniSettings.setString(self.SECTION_INTERFACE, "UsePanicHandlers", "False")
        # Don't confirm at stop
        iniSettings.setString(self.SECTION_INTERFACE, "ConfirmStop", "False")
        # System Language 
        iniSettings.setInt(self.SECTION_INTERFACE, "Language", gamecubeLanguage)
        # only 1 window (fixes exit and gui display)
        iniSettings.setString(self.SECTION_DISPLAY, "RenderToMain", "True")
        iniSettings.setString(self.SECTION_DISPLAY, "Fullscreen", "True")
        # Enable Cheats
        iniSettings.setString(self.SECTION_CORE, "EnableCheats", "True")
        #Dual Core
        iniSettings.setString(self.SECTION_CORE, "CPUThread", "True")
        # Gpu Sync
        #iniSettings.setString(self.SECTION_CORE, "SyncGPU", "True")
        # Language
        iniSettings.setInt(self.SECTION_CORE, "SelectedLanguage", gamecubeLanguage)
        #iniSettings.setString(self.SECTION_CORE, "SelectedLanguage", str(getGameCubeLangFromEnvironment())) # Wii
        #iniSettings.setString(self.SECTION_CORE, "GameCubeLanguage", str(getGameCubeLangFromEnvironment())) # GC
        # Enable MMU
        iniSettings.setString(self.SECTION_CORE, "MMU", "True")
        # Backend - Default OpenGL
        iniSettings.setString(self.SECTION_CORE, "GFXBackend", "OGL")

        # Serial Port 1 to AM-Baseband
        iniSettings.setString(self.SECTION_CORE, "SerialPort1", "6")
        # Gamecube pads forced as AM-Baseband
        iniSettings.setString(self.SECTION_CORE, "SIDevice0", "11")

        # F-Zero GX exception, it needs to not be using the AM-Baseband to function.
        # This cannot be set in the game's INI for some reason.
        # if args.rom == "F-Zero GX (USA).iso":
        #     dolphinTriforceSettings.set("Core", "SerialPort1", "255")
        # else:
        #     dolphinTriforceSettings.set("Core", "SerialPort1", "6")

        # # Gamecube pads forced as AM-Baseband
        # # F-Zero GX exception, it needs it to be a regular pad instead.
        # if args.rom == "F-Zero GX (USA).iso":
        #     dolphinTriforceSettings.set("Core", "SIDevice0", "6")
        # else:
        #     dolphinTriforceSettings.set("Core", "SIDevice0", "11")
        
        # Save configuration
        iniSettings.saveFile()

        # Setting on gfx_opengl.ini
        # Load Configuration
        from configgen.settings.iniSettings import IniSettings
        GFXSettings = IniSettings(recalboxFiles.dolphinTriforceGfxIni, True)
        GFXSettings.loadFile(True)
            
        # Get Ratio
        gameRatio = self.GAME_RATIO[system.Ratio] if system.Ratio in self.GAME_RATIO else 0
        GFXSettings.setInt(self.SECTION_SETTINGS, "AspectRatio", gameRatio)
        if gameRatio == self.GAME_RATIO["16/9"]:
            GFXSettings.setString(self.SECTION_SETTINGS, "wideScreenHack", "True")
        # # Show fps
        GFXSettings.setBool(self.SECTION_SETTINGS, "ShowFPS", system.ShowFPS)
        # HiResTextures and CacheHiresTextures beware don't activate for load StateSaves !!!!!
        #GFXSettings.setString(self.SECTION_SETTINGS, "HiresTextures",      "True")
        #GFXSettings.setString(self.SECTION_SETTINGS, "CacheHiresTextures", "True")
        # VSync
        GFXSettings.setString(self.SECTION_HARDWARE, "VSync", "True")

        # # Widescreen Hack
        # if system.isOptSet('widescreen_hack') and system.getOptBoolean('widescreen_hack'):
        #     # Prefer Cheats than Hack 
        #     if system.isOptSet('enable_cheats') and system.getOptBoolean('enable_cheats'):
        #         dolphinTriforceGFXSettings.set("Settings", "wideScreenHack", "False")
        #     else:
        #         dolphinTriforceGFXSettings.set("Settings", "wideScreenHack", "True")
        # else:
        #     dolphinTriforceGFXSettings.set("Settings", "wideScreenHack", "False")

        # Various performance hacks - Default Off
        GFXSettings.setString(self.SECTION_HACKS, "BBoxEnable", "False")
        GFXSettings.setString(self.SECTION_HACKS, "DeferEFBCopies", "True")
        GFXSettings.setString(self.SECTION_HACKS, "EFBEmulateFormatChanges", "False")
        GFXSettings.setString(self.SECTION_HACKS, "EFBScaledCopy", "True")
        GFXSettings.setString(self.SECTION_HACKS, "EFBToTextureEnable", "True")
        GFXSettings.setString(self.SECTION_HACKS, "SkipDuplicateXFBs", "True")
        GFXSettings.setString(self.SECTION_HACKS, "XFBToTextureEnable", "True")
        GFXSettings.setString(self.SECTION_ENHANCEMENTS, "ForceFiltering", "True")
        GFXSettings.setString(self.SECTION_ENHANCEMENTS, "ArbitraryMipmapDetection", "True")
        GFXSettings.setString(self.SECTION_ENHANCEMENTS, "DisableCopyFilter", "True")
        GFXSettings.setString(self.SECTION_ENHANCEMENTS, "ForceTrueColor", "True")
        # else:
        #     if dolphinTriforceGFXSettings.has_section("Hacks"):
        #         dolphinTriforceGFXSettings.remove_option("Hacks", "BBoxEnable")
        #         dolphinTriforceGFXSettings.remove_option("Hacks", "DeferEFBCopies")
        #         dolphinTriforceGFXSettings.remove_option("Hacks", "EFBEmulateFormatChanges")
        #         dolphinTriforceGFXSettings.remove_option("Hacks", "EFBScaledCopy")
        #         dolphinTriforceGFXSettings.remove_option("Hacks", "EFBToTextureEnable")
        #         dolphinTriforceGFXSettings.remove_option("Hacks", "SkipDuplicateXFBs")
        #         dolphinTriforceGFXSettings.remove_option("Hacks", "XFBToTextureEnable")
        #     if dolphinTriforceGFXSettings.has_section("Enhancements"):
        #         dolphinTriforceGFXSettings.remove_option("Enhancements", "ForceFiltering")
        #         dolphinTriforceGFXSettings.remove_option("Enhancements", "ArbitraryMipmapDetection")
        #         dolphinTriforceGFXSettings.remove_option("Enhancements", "DisableCopyFilter")
        #         dolphinTriforceGFXSettings.remove_option("Enhancements", "ForceTrueColor")

        # Save Settings
        GFXSettings.saveFile()

        # Setting on Logger.ini
        # Load Configuration
        from configgen.settings.iniSettings import IniSettings
        LogSettings = IniSettings(recalboxFiles.dolphinTriforceLoggerIni, True)
        LogSettings.loadFile(True)

        # Prevent the constant log spam.
        LogSettings.setString(self.SECTION_LOGS, "DVD", "False")

        # Save Logger.ini
        LogSettings.saveFile()
        
        #read gameID from iso file
        Log("from {} :".format(args.rom));
        file = open(args.rom, "rb")
        gameID = file.read(6).decode('utf-8')
        Log(gameID)
        file.close()

        #prepare and clear card if needed 
        if gameID in self.GAME_NEED_CLEANCARD_FIRST:
            #delete file if exists here /recalbox/share/system/.dolphin-tri/Triforce/tricard_{GAMEID}.bin
            Log("Clean card (if exists) before loading of this game !")
            if os.path.exists("/recalbox/share/system/.dolphin-tri/Triforce/tricard_{}.bin".format(gameID)):
                Log("Delete file /recalbox/share/system/.dolphin-tri/Triforce/tricard_{}.bin".format(gameID))
                os.remove("/recalbox/share/system/.dolphin-tri/Triforce/tricard_{}.bin".format(gameID))

        #prepare and send F1 command to load 
        if gameID in self.GAME_NEED_SAVESATE_FIRST:
            #load the default save state (F1 key)
            #wait 5s and simulate press on F1
            Log("Activate auto save state loading for this game !")
            p = subprocess.Popen('xdotool sleep 7 key F1 &', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)                    
            
        commandArray = [recalboxFiles.recalboxBins[system.Emulator], "-b", "-U", recalboxFiles.dolphinTriforceConfig, "-e", args.rom]
        return Command(videomode=system.VideoMode, array=commandArray, env={"XDG_CONFIG_HOME":recalboxFiles.CONF, "XDG_DATA_HOME":recalboxFiles.SAVES})
