#!/usr/bin/env python

import subprocess
import json
import re
import os
import evdev
from configgen.controllers.inputItem import InputItem

def Log(txt):
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(txt)
    return 0
    
class Evmapy():
    # evmapy is a process that map pads to keyboards (for pygame for example)
    __started = False

    @staticmethod
    def start(system, emulator, core, rom, playersControllers):
        if Evmapy.__prepare(system, emulator, core, rom, playersControllers):
            Evmapy.__started = True
            subprocess.call(["pixl-evmapy", "start"])

    @staticmethod
    def stop():
        if Evmapy.__started:
            Evmapy.__started = False
            subprocess.call(["pixl-evmapy", "stop"])

    @staticmethod
    def __prepare(system, emulator, core, rom, playersControllers):
        # consider files here in this order to get a configuration
        for keysfile in [
                "{}.keys" .format (rom),
                "{}/padto.keys" .format (rom), # case when the rom is a directory
                "/recalbox/share/system/configs/evmapy/{}.{}.{}.keys" .format (system, emulator, core),
                "/recalbox/share/system/configs/evmapy/{}.{}.keys" .format (system, emulator),
                "/recalbox/share/system/configs/evmapy/{}.keys" .format (system),
                "/recalbox/share/system/configs/evmapy/{}.{}.keys" .format (emulator,core),
                "/recalbox/share/system/configs/evmapy/{}.keys" .format (emulator)
        ]:
            Log("keysfile path : {}".format(keysfile))
            if os.path.exists(keysfile) and not (os.path.isdir(rom) and keysfile == "{}.keys" .format (rom)): # "{}.keys" .format (rom) is forbidden for directories, it must be inside
                #eslog.debug("evmapy on {}".format(keysfile))
                Log("evmapy on {}".format(keysfile))
                subprocess.call(["pixl-evmapy", "clear"])
    
                padActionConfig = json.load(open(keysfile))
    
                # configure each player
                nplayer = 1
                for playercontroller, pad in sorted(playersControllers.items()):
                    if "actions_player"+str(nplayer) in padActionConfig:
                        Log("pad.DevicePath : {}".format(pad.DevicePath))
                        configfile = "/var/run/evmapy/{}.json" .format (os.path.basename(pad.DevicePath))
                        #eslog.debug("config file for keysfile is {} (from {})" .format (configfile, keysfile))
                        Log("config file for keysfile is {} (from {})" .format (configfile, keysfile))

                        # create mapping
                        padConfig = {}
                        padConfig["axes"] = []
                        padConfig["buttons"] = []
                        padConfig["grab"] = False
                        absbasex_positive = True
                        absbasey_positive = True

                        # define buttons / axes
                        known_buttons_names = {}
                        known_buttons_codes = {}
                        known_buttons_alias = {}
                        known_axes_codes = {}
                        #bato for index in pad.inputs:
                        for input in pad.AvailableInput:
                            #bato input = pad.inputs[index]
                            Log("input.Type is {}" .format (input.Type))
                            #bato if input.type == "button":
                            if input.IsButton:
                                # don't add 2 times the same button (ie select as hotkey)
                                Log("input.Code is {}" .format (input.Code))
                                #bato if input.code is not None:
                                if input.Code is not None:
                                    #bato if input.code not in known_buttons_codes:
                                    if input.Code not in known_buttons_codes:
                                        #bato known_buttons_names[input.name] = True
                                        known_buttons_names[input.Name] = True
                                        #bato known_buttons_codes[input.code] = input.name # keep the master name for aliases
                                        known_buttons_codes[input.Code] = input.Name # keep the master name for aliases
                                        Log("input.Name is {}" .format (input.Name))
                                        padConfig["buttons"].append({
                                            #bato "name": input.name,
                                            #bato "code": int(input.code)
                                            "name": input.Name,
                                            "code": int(input.Code)
                                        })
                                    else:
                                        #bato known_buttons_alias[input.name] = known_buttons_codes[input.code]
                                        known_buttons_alias[input.Name] = known_buttons_codes[input.Code]
                            #bato elif input.type == "hat":
                            elif input.IsHat:
                                #bato if int(input.value) in [1, 2]: # don't duplicate values
                                Log("input.Value is {}" .format (input.Value))
                                if int(input.Value) in [1, 2]: # don't duplicate values
                                    #bato if int(input.value) == 1:
                                    if int(input.Value) == 1:
                                        name = "X"
                                        isYAsInt = 0
                                    else:
                                        name = "Y"
                                        isYAsInt =  1
                                    Log("input.Id is {}" .format (input.Id))
                                    #bato known_buttons_names["HAT" + input.id + name + ":min"] = True
                                    known_buttons_names["HAT" + str(input.Id) + name + ":min"] = True
                                    #bato known_buttons_names["HAT" + input.id + name + ":max"] = True
                                    known_buttons_names["HAT" + str(input.Id) + name + ":max"] = True
                                    padConfig["axes"].append({
                                        #bato "name": "HAT" + input.id + name,
                                        "name": "HAT" + str(input.Id) + name,
                                        #bato "code": int(input.id) + 16 + isYAsInt, # 16 = HAT0X in linux/input.h
                                        "code": int(input.Id) + 16 + isYAsInt, # 16 = HAT0X in linux/input.h
                                        "min": -1,
                                        "max": 1
                                    })
                            #bato elif input.type == "axis":
                            elif input.IsAxis:
                                Log("input.Code is {}" .format (input.Code))
                                #bato if input.code not in known_axes_codes: # avoid duplicated value for axis (bad pad configuration that make evmappy to stop)
                                if input.Code not in known_axes_codes: # avoid duplicated value for axis (bad pad configuration that make evmappy to stop)
                                    #bato known_axes_codes[input.code] = True
                                    known_axes_codes[input.Code] = True
                                    axisId = None
                                    axisName = None
                                    Log("input.Name is {}" .format (input.Name))
                                    #bato if input.name == "joystick1up" or input.name == "joystick1left":
                                    if input.Name == "joystick1up" or input.Name == "joystick1left":
                                        axisId = "0"
                                    #bato elif input.name == "joystick2up" or input.name == "joystick2left":
                                    elif input.Name == "joystick2up" or input.Name == "joystick2left":
                                        axisId = "1"
                                    #bato if input.name == "joystick1up" or input.name == "joystick2up":
                                    if input.Name == "joystick1up" or input.Name == "joystick2up":
                                        axisName = "Y"
                                    #bato elif input.name == "joystick1left" or input.name == "joystick2left":
                                    elif input.Name == "joystick1left" or input.Name == "joystick2left":
                                        axisName = "X"
                                    #bato elif input.name == "up" or input.name == "down":
                                    elif input.Name == "up" or input.Name == "down":
                                        axisId   = "BASE"
                                        axisName = "Y"
                                        #bato if input.name == "up":
                                        if input.Name == "up":
                                            #bato absbasey_positive =  int(input.value) >= 0
                                            absbasey_positive =  int(input.Value) >= 0
                                        else:
                                            axisId = None # don't duplicate, configuration should be done for up
                                    #bato elif input.name == "left" or input.name == "right":
                                    elif input.Name == "left" or input.Name == "right":
                                        axisId   = "BASE"
                                        axisName = "X"
                                        #bato if input.name == "left":
                                        if input.Name == "left":
                                            #bato absbasex_positive = int(input.value) < 0
                                            absbasex_positive = int(input.Value) < 0
                                        else:
                                            axisId = None # don't duplicate, configuration should be done for left
                                    else:
                                        axisId   = "_OTHERS_"
                                        #bato axisName = input.name
                                        axisName = input.Name

                                    #bato if ((axisId in ["0", "1", "BASE"] and axisName in ["X", "Y"]) or axisId == "_OTHERS_") and input.code is not None:
                                    if ((axisId in ["0", "1", "BASE"] and axisName in ["X", "Y"]) or axisId == "_OTHERS_") and input.Code is not None:
                                        #bato axisMin, axisMax = Evmapy.__getPadMinMaxAxis(pad.dev, int(input.code))
                                        axisMin, axisMax = Evmapy.__getPadMinMaxAxis(pad.DevicePath, int(input.Code))
                                        known_buttons_names["ABS" + axisId + axisName + ":min"] = True
                                        known_buttons_names["ABS" + axisId + axisName + ":max"] = True
                                        known_buttons_names["ABS" + axisId + axisName + ":val"] = True

                                        padConfig["axes"].append({
                                            "name": "ABS" + axisId + axisName,
                                            #bato "code": int(input.code),
                                            "code": int(input.Code),
                                            "min": axisMin,
                                            "max": axisMax
                                        })

                        # only add actions for which buttons are defined (otherwise, evmapy doesn't like it)
                        padActionsPreDefined = padActionConfig["actions_player"+str(nplayer)]
                        padActionsFiltered = []

                        # handle mouse events : only joystick1 or joystick2 defined for 2 events
                        padActionsDefined = []
                        for action in padActionsPreDefined:
                            if "type" in action and action["type"] == "mouse" and "target" not in action and "trigger" in action:
                                if action["trigger"] == "joystick1":
                                    newaction = action.copy()
                                    newaction["trigger"] = "joystick1x"
                                    newaction["target"] = 'X'
                                    padActionsDefined.append(newaction)
                                    newaction = action.copy()
                                    newaction["trigger"] = "joystick1y"
                                    newaction["target"] = 'Y'
                                    padActionsDefined.append(newaction)
                                elif action["trigger"] == "joystick2":
                                    newaction = action.copy()
                                    newaction["trigger"] = "joystick2x"
                                    newaction["target"] = 'X'
                                    padActionsDefined.append(newaction)
                                    newaction = action.copy()
                                    newaction["trigger"] = "joystick2y"
                                    newaction["target"] = 'Y'
                                    padActionsDefined.append(newaction)
                            else:
                                padActionsDefined.append(action)

                        # define actions
                        for action in padActionsDefined:
                            if "trigger" in action:
                                trigger = Evmapy.__trigger_mapper(action["trigger"], known_buttons_alias, known_buttons_names, absbasex_positive, absbasey_positive)
                                if "mode" not in action:
                                    mode = Evmapy.__trigger_mapper_mode(action["trigger"])
                                    if mode != None:
                                        action["mode"] = mode
                                action["trigger"] = trigger
                                if isinstance(trigger, list):
                                    allfound = True
                                    for x in trigger:
                                        if x not in known_buttons_names and ("ABS_OTHERS_" + x + ":max") not in known_buttons_names :
                                            allfound = False
                                    if allfound:
                                        # rewrite axis buttons
                                        x = 0
                                        for val in trigger:
                                            if "ABS_OTHERS_" + val + ":max" in known_buttons_names:
                                                action["trigger"][x] = "ABS_OTHERS_" + val + ":max"
                                            x = x+1
                                        padActionsFiltered.append(action)
                                else:
                                    if trigger in known_buttons_names:
                                        padActionsFiltered.append(action)
                                    if "ABS_OTHERS_" + trigger + ":max" in known_buttons_names:
                                        action["trigger"] = "ABS_OTHERS_" + action["trigger"] + ":max"
                                        padActionsFiltered.append(action)
                                padConfig["actions"] = padActionsFiltered

                        # remove comments
                        for action in padConfig["actions"]:
                            if "description" in action:
                                del action["description"]

                        # use full axis for mouse and 50% for keys
                        axis_for_mouse = {}
                        for action in padConfig["actions"]:
                            if "type" in action and action["type"] == "mouse":
                                if isinstance(action["trigger"], list):
                                    for x in action["trigger"]:
                                        axis_for_mouse[x] = True
                                else:
                                    axis_for_mouse[action["trigger"]] = True

                        for axis in padConfig["axes"]:
                            if axis["name"]+":val" not in axis_for_mouse and axis["name"]+":min" not in axis_for_mouse and axis["name"]+":max" not in axis_for_mouse:
                                min, max = Evmapy.__getPadMinMaxAxisForKeys(axis["min"], axis["max"])
                                axis["min"] = min
                                axis["max"] = max

                        # save config file
                        with open(configfile, "w") as fd:
                            fd.write(json.dumps(padConfig, indent=4))
    
                    nplayer += 1
                return True
        # otherwise, preparation did nothing
        return False
    
    # remap evmapy trigger (aka up become HAT0Y:max)
    @staticmethod
    def __trigger_mapper(trigger, known_buttons_alias, known_buttons_names, absbasex_positive, absbasey_positive):
        if isinstance(trigger, list):
            new_trigger = []
            for x in trigger:
                new_trigger.append(Evmapy.__trigger_mapper_string(x, known_buttons_alias, known_buttons_names, absbasex_positive, absbasey_positive))
            return new_trigger
        return Evmapy.__trigger_mapper_string(trigger, known_buttons_alias, known_buttons_names, absbasex_positive, absbasey_positive)

    @staticmethod
    def __trigger_mapper_string(trigger, known_buttons_alias, known_buttons_names, absbasex_positive, absbasey_positive):
        # maybe this function is more complex if a pad has several hat. never see them.
        mapping = {
            "joystick1right": "ABS0X:max",
            "joystick1left": "ABS0X:min",
            "joystick1down": "ABS0Y:max",
            "joystick1up": "ABS0Y:min",
            "joystick2right": "ABS1X:max",
            "joystick2left": "ABS1X:min",
            "joystick2down": "ABS1Y:max",
            "joystick2up": "ABS1Y:min",
            "joystick1x": ["ABS0X:val", "ABS0X:min", "ABS0X:max"],
            "joystick1y": ["ABS0Y:val", "ABS0Y:min", "ABS0Y:max"],
            "joystick2x": ["ABS1X:val", "ABS1X:min", "ABS1X:max"],
            "joystick2y": ["ABS1Y:val", "ABS1Y:min", "ABS1Y:max"]
        }

        if "HAT0X:min" in known_buttons_names:
            mapping["left"]  = "HAT0X:min"
            mapping["right"] = "HAT0X:max"
            mapping["down"]  = "HAT0Y:max"
            mapping["up"]    = "HAT0Y:min"

        if "ABSBASEX:min" in known_buttons_names:
            if absbasex_positive:
                mapping["left"]  = "ABSBASEX:min"
                mapping["right"] = "ABSBASEX:max"
            else:
                mapping["left"]  = "ABSBASEX:max"
                mapping["right"] = "ABSBASEX:min"

        if "ABSBASEX:min" in known_buttons_names:
            if absbasey_positive:
                mapping["down"]  = "ABSBASEY:max"
                mapping["up"]    = "ABSBASEY:min"
            else:
                mapping["down"]  = "ABSBASEY:min"
                mapping["up"]    = "ABSBASEY:max"

        if trigger in known_buttons_alias:
            return known_buttons_alias[trigger]
        if trigger in mapping:
            if isinstance(mapping[trigger], list):
                all_found = True
                for x in mapping[trigger]:
                    if x not in known_buttons_names:
                        all_found = False
                if all_found:
                    return mapping[trigger]
            elif mapping[trigger] in known_buttons_names:
                return mapping[trigger]
        return trigger # no tranformation

    @staticmethod
    def __trigger_mapper_mode(trigger):
        if isinstance(trigger, list):
            new_trigger = []
            for x in trigger:
                mode = Evmapy.__trigger_mapper_mode_string(x)
                if mode != None:
                    return mode
            return None
        return Evmapy.__trigger_mapper_mode_string(trigger)

    @staticmethod
    def __trigger_mapper_mode_string(trigger):
        if trigger in [ "joystick1x", "joystick1y", "joystick2x", "joystick2y"]:
            return "any"
        return None

    @staticmethod
    def __getPadMinMaxAxis(devicePath, axisCode):
        device = evdev.InputDevice(devicePath)
        capabilities = device.capabilities(False)

        for event_type in capabilities:
            if event_type == 3: # "EV_ABS"
                for abs_code, val in capabilities[event_type]:
                    if abs_code == axisCode:
                        return val.min, val.max
        return 0,0 # not found

    @staticmethod
    def __getPadMinMaxAxisForKeys(min, max):
        valrange = (max - min)/2 # for each side
        valmin   = min + valrange/2
        valmax   = max - valrange/2
        return valmin, valmax
