#!/usr/bin/env python
HOME_INIT = '/recalbox/share_init/system/'
HOME = '/recalbox/share/system'
CONF = HOME + '/configs'
CACHE = HOME + "/.cache"
SAVES = '/recalbox/share/saves'
SAVES_INIT = '/recalbox/share_init/saves'
SCREENSHOTS = '/recalbox/share/screenshots'
BIOS = '/recalbox/share/bios'
BIOS_INIT = '/recalbox/share_init/bios'
OVERLAYS = '/recalbox/share/overlays'
RECALBOX_OVERLAYS = '/recalbox/share_init/overlays'
RECALBOX_240P_OVERLAYS = '/recalbox/share_init/240poverlays'
ROMS = '/recalbox/share/roms'

pegasusInputs = HOME + '/.pegasus-frontend/input.cfg'
recalboxConf = HOME + '/recalbox.conf'
logdir = HOME + '/logs/'

pegasusLightGun = HOME_INIT + '/.pegasus-frontend/lightgun.cfg'
GameListFileName = "gamelist.xml"

# This dict is indexed on the emulator name, not on the system
recalboxBins =\
{
    'advancemame'       : '/usr/bin/advmame',
    'amiberry'          : '/usr/bin/amiberry',
    'beebem'            : '/usr/bin/Beebem',
    'citra'             : '/usr/bin/citra',
    'daphne'            : '/usr/bin/hypseus',
    'dolphin'           : '/usr/bin/dolphin-emu',
    'dolphin-triforce'  : '/usr/bin/dolphin-triforce',
    'dosbox'            : '/usr/bin/dosbox',
    'duckstation'       : '/usr/bin/duckstation',
    'fba2x'             : '/usr/bin/fba2x',
    'frotz'             : '/usr/bin/sfrotz',
    'gsplus'            : '/usr/bin/GSplus',
    'hatari'            : '/usr/bin/hatari',
    'libretro'          : '/usr/bin/retroarch',
    'linapple'          : '/usr/bin/linapple',
    'mame'              : '/usr/bin/mame/mame',
    'mupen64plus'       : '/usr/bin/mupen64plus',
    'openbor'           : '/usr/bin/OpenBOR',
    'oricutron'         : '/usr/bin/oricutron/oricutron',
    'pcsx_rearmed'      : '/usr/bin/pcsx_rearmed',
    'pcsx2'             : '/usr/bin/PCSX2/pcsx2',
    'pisnes'            : '/usr/bin/pisnes',
    'ppsspp'            : '/usr/bin/PPSSPPSDL',
    'rb5000'            : '/usr/bin/rb5000',
    'reicast'           : '/usr/bin/reicast.elf',
    'scummvm'           : '/usr/bin/scummvm',
    'simcoupe'          : '/usr/bin/simcoupe',
    'solarus'           : '/usr/bin/solarus-run',
    'supermodel'        : '/usr/bin/supermodel',
    'ti99sim'           : '/usr/bin/ti99sim/ti99sim-sdl',
    'vice'              : '/usr/bin/x64',
    'xroar'             : '/usr/bin/xroar',
    'xemu'              : '/usr/bin/xemu',
}


retroarchRoot = CONF + '/retroarch'
retroarchCustom = retroarchRoot + '/retroarchcustom.cfg'
retroarchCustomOrigin = retroarchRoot + "/retroarchcustom.cfg.origin"
retroarchCoreCustom = retroarchRoot + "/cores/retroarch-core-options.cfg"
retroarchInitCustomOrigin = HOME_INIT + "configs/retroarch/retroarchcustom.cfg.origin"

retroarchCores = "/usr/lib/libretro/"
shadersRoot = "/recalbox/share/shaders/"
shadersExt = '.gplsp'
libretroExt = '_libretro.so'
screenshotsDir = "/recalbox/share/screenshots/"
savesDir = "/recalbox/share/saves/"

fbaRoot = CONF + '/fba/'
fbaCustom = fbaRoot + 'fba2x.cfg'
fbaCustomOrigin = fbaRoot + 'fba2x.cfg.origin'


mupenConf = CONF + '/mupen64/'
mupenCustom = mupenConf + "mupen64plus.cfg"
mupenInput = mupenConf + "InputAutoCfg.ini"
mupenSaves = SAVES + "/n64"
mupenMappingUser    = mupenConf + 'input.xml'
mupenMappingSystem  = '/recalbox/share_init/system/configs/mupen64/input.xml'

shaderPresetRoot = "/recalbox/share_init/system/configs/shadersets/"

reicastCustom = CONF + '/reicast'
reicastConfig = reicastCustom + '/emu.cfg'
reicastConfigInit = HOME_INIT + 'configs/reicast/emu.cfg'
reicastSaves = SAVES + '/dreamcast'
reicastBios = BIOS

dolphinConfig  = CONF + "/dolphin-emu"
dolphinData    = SAVES + "/dolphin-emu"
dolphinIni     = dolphinConfig + '/Dolphin.ini'
dolphinHKeys   = dolphinConfig + '/Hotkeys.ini'
dolphinGFX     = dolphinConfig + '/GFX.ini'
dolphinSYSCONF = dolphinData + '/Wii/shared2/sys/SYSCONF'

ppssppConf = CONF + '/ppsspp/PSP/SYSTEM'
ppssppControlsIni = ppssppConf + '/controls.ini'
ppssppControls = CONF + '/ppsspp/gamecontrollerdb.txt'
ppssppControlsInit = HOME_INIT + 'configs/ppsspp/PSP/SYSTEM/controls.ini'
ppssppConfig = ppssppConf + '/ppsspp.ini'
ppssppSaves = SAVES + '/psp'

dosboxCustom = CONF + '/dosbox'
dosboxConfig = dosboxCustom + '/dosbox.conf'

scummvmSaves = SAVES + '/scummvm'

simcoupeConfig = HOME + '/.simcoupe/SimCoupe.cfg'

viceConfig = CONF + "/vice/vice.conf"

advancemameConfig = CONF + '/advancemame/advmame.rc'
advancemameConfigOrigin = CONF + '/advancemame/advmame.rc.origin'

amiberryMountPoint = '/tmp/amiga'
amiberrySaves      = SAVES

daphneInputIni = CONF + '/daphne/dapinput.ini'
daphneHomedir = ROMS + '/daphne'
daphneDatadir = '/usr/share/daphne'

oricutronConfig = HOME + '/.config/oricutron.cfg'

openborConfig = HOME + '/configs/openbor/default.cfg'
openborConfigOrigin = HOME + '/configs/openbor/default.cfg.origin'

gsplusConfig = HOME + '/.config/gsplus.cfg'

atari800CustomConfig = HOME + '/.atari800.cfg'

hatariCustomConfig = HOME + '/.hatari/hatari.cfg'

pcsxRootFolder = '/recalbox/share/system/configs/pcsx'
pcsxConfigFile = pcsxRootFolder + '/pcsx.cfg'

pisnesRootFolder = '/recalbox/share/system/configs/pisnes'
pisnesConfigFile = pisnesRootFolder + '/snes9x.cfg'

supermodelRooFolder = CONF + '/model3'
supermodelConfigFile = supermodelRooFolder + '/ConfigModel3.ini'
supermodelControlsIni = supermodelRooFolder + '/Supermodel.ini'

crtFilesRootFolder = '/recalbox/system/configs/crt/'
crtUserFilesRootFolder = '/recalbox/share/system/configs/crt/'

citraConfig = CONF + "/citra-emu"
citraData   = SAVES + "/3ds"
citraIni    = citraConfig + '/qt-config.ini'
citraSysConf = citraData + '/nand/data/00000000000000000000000000000000/sysdata/00010017/00000000/config'

dolphinTriforceConfig  = CONF + "/dolphin-triforce"
dolphinTriforceData    = SAVES + "/dolphin-triforce"
dolphinTriforceIni     = dolphinTriforceConfig + '/Config/Dolphin.ini'
dolphinTriforceGfxIni  = dolphinTriforceConfig + '/Config/gfx_opengl.ini'
dolphinTriforceLoggerIni    = dolphinTriforceConfig + '/Config/Logger.ini'
dolphinTriforceGameSettings = dolphinTriforceConfig + "/GameSettings"

xemuConfig = CONF + "/xemu"
xemuIni = xemuConfig + "/xemu.ini"

minivmacRomFile = '/tmp/minivmac.cmd'
minivmacOsFile = BIOS + '/macintosh/MinivMacBootv2.dsk'

frotzConfig = HOME + '/.config/frotz/frotz.conf'
