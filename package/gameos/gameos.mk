################################################################################
#
# pixL themes for Pegasus : https://github.com/pixl-os/gameOS
#
################################################################################

# commit from pixL-master
# https://github.com/pixl-os/gameOS/
# version 1.22 - 2023-04-29
GAMEOS_VERSION = 0ac33a77014c81f229d21cf27b0153e47322f6a3
GAMEOS_SITE = $(call github,pixl-os,gameOS,$(GAMEOS_VERSION))

define GAMEOS_INSTALL_TARGET_CMDS
	# create directories if not already done
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes/gameOS
	# copy theme files in share_init
	cp -R $(@D)/* $(TARGET_DIR)/recalbox/share_init/themes/gameOS
	# remove project files
	find $(TARGET_DIR)/recalbox/share_init/themes/gameOS -name "*.pro" | xargs rm
	find $(TARGET_DIR)/recalbox/share_init/themes/gameOS -name "*.ts" | xargs rm
	find $(TARGET_DIR)/recalbox/share_init/themes/gameOS -name "CODEOWNERS" | xargs rm
endef

$(eval $(generic-package))
