################################################################################
#
# redshift
#
################################################################################

REDSHIFT_VERSION = 490ba2aae9cfee097a88b6e2be98aeb1ce990050
REDSHIFT_SITE = https://github.com/jonls/redshift.git
REDSHIFT_LICENSE = GPLv2
REDSHIFT_INSTALL_STAGING = YES
REDSHIFT_SITE_METHOD=git
REDSHIFT_GIT_SUBMODULES = YES
REDSHIFT_SUPPORTS_IN_SOURCE_BUILD = NO
REDSHIFT_DEPENDENCIES = host-intltool host-gettext-tiny gettext-tiny libtool

define REDSHIFT_PRE_CONFIGURE
	$(SED) "s|intltoolize|$(HOST_DIR)/bin/intltoolize|g" $(@D)/bootstrap
	$(@D)/./bootstrap --prefix=$(HOST_DIR)/bin
	$(@D)/./configure	
endef

REDSHIFT_PRE_CONFIGURE_HOOKS += REDSHIFT_PRE_CONFIGURE

$(eval $(autotools-package))