################################################################################
#
# DOLPHIN TRIFORCE
#
################################################################################
# This is a build that is no longer receiving updates. AppImage format ensures compatibility into the future.
# Site https://dolphin-emu.org/download/list/Triforce/1/
# Source https://github.com/EIGHTFINITE/dolphin-triforce
# Dolphin version: 4.0-315
DOLPHIN_TRIFORCE_VERSION_CORE = 4.0-315
DOLPHIN_TRIFORCE_SOURCE = 
DOLPHIN_TRIFORCE_VERSION = 1.0.0-001
DOLPHIN_TRIFORCE_LICENSE = GPLv2+
DOLPHIN_TRIFORCE_DEPENDENCIES = xserver_xorg-server

## print version of core in PixL
define DOLPHIN_TRIFORCE_PRE_CONFIGURE
	echo "Dolphin-triforce;dolphin-triforce;${DOLPHIN_TRIFORCE_VERSION_CORE}" > $(TARGET_DIR)/recalbox/share_init/system/configs/dolphin-triforce.corenames
endef
DOLPHIN_TRIFORCE_PRE_CONFIGURE_HOOKS += DOLPHIN_TRIFORCE_PRE_CONFIGURE

# Includes custom game configs required to successfully launch and play them.
define DOLPHIN_TRIFORCE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/bin
	$(INSTALL) -D -m 0555 "$(BR2_EXTERNAL_RECALBOX_PATH)/package/dolphin-triforce/dolphin-triforce.AppImage" \
		"${TARGET_DIR}/usr/bin/dolphin-triforce"
endef

# Hotkeys using evmapy
define DOLPHIN_TRIFORCE_EVMAP
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/dolphin-triforce/triforce.dolphin-triforce.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef
DOLPHIN_TRIFORCE_POST_INSTALL_TARGET_HOOKS = DOLPHIN_TRIFORCE_EVMAP

$(eval $(generic-package))
