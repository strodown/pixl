################################################################################
#
# SLANG_SHADERS
#
################################################################################

# Commits on May 4, 2023
LIBRETRO_SLANG_SHADERS_VERSION = b1d6061d25681316bd70c09309cb6f5c155ab3d8
LIBRETRO_SLANG_SHADERS_SITE = $(call github,libretro,slang-shaders,$(LIBRETRO_SLANG_SHADERS_VERSION))
LIBRETRO_SLANG_SHADERS_LICENSE = MIT
LIBRETRO_SLANG_SHADERS_LICENSE_FILES = COPYING

define LIBRETRO_SLANG_SHADERS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/recalbox/share_init/shaders/
	cp -a $(@D)/* \
			$(TARGET_DIR)/recalbox/share_init/shaders/
	rm -f $(TARGET_DIR)/recalbox/share_init/shaders/Makefile \
		$(TARGET_DIR)/recalbox/share_init/shaders/configure
endef

$(eval $(generic-package))
