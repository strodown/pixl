########################################################################################################################
#
# PCSX2
#
########################################################################################################################

########################### these versions of build tested need QT6 and stop usage of wxWidgets ########################
#Version: Commits on feb 19, 2022 -> nok - fatal error: rc_api_info.h: No such file or directory
#PCSX2_VERSION = v1.7.4118
#Version: Commits on feb 18, 2022 -> nok - fatal error: rc_api_info.h: No such file or directory
#PCSX2_VERSION = v1.7.4107 
#Version: Commits on Jan 14, 2022 -> nok - fatal error: rc_api_info.h: No such file or directory
#PCSX2_VERSION = v1.7.3900
#Version: Commits on Dec 28, 2022  -> nok - fatal error: rc_api_info.h: No such file or directory
#PCSX2_VERSION = v1.7.3800
#Version: Commits on Dec 24, 2022  > nok - fatal error: rc_api_info.h: No such file or directory
#PCSX2_VERSION = v1.7.3775
#PCSX2_VERSION = v1.7.3774
#PCSX2_VERSION = v1.7.3773
#######################################################################################################################

########################### these versions of build tested still to use wxWidgets #########################################
#Version: Commits on Dec 24, 2022  -> pass with -DDISABLE_ADVANCE_SIMD=OFF
#PCSX2_VERSION = v1.7.3772
#PCSX2_VERSION = v1.7.3770
#PCSX2_VERSION = v1.7.3760
#PCSX2_VERSION = v1.7.3750

#Version: Commits on Dec 3, 2022  -> pass with -DDISABLE_ADVANCE_SIMD=OFF
#PCSX2_VERSION = v1.7.3670

#Version: Commits on Dec 3, 2022 -> work with PCSX2_CONF_OPTS += -DDISABLE_ADVANCE_SIMD=ON
#PCSX2_VERSION = v1.7.3668

#Version: Commits on Dec 2, 2022 -> work with PCSX2_CONF_OPTS += -DDISABLE_ADVANCE_SIMD=ON
#PCSX2_VERSION = v1.7.3665

#Version: Commits on Nov 27, 2022 -> work with PCSX2_CONF_OPTS += -DDISABLE_ADVANCE_SIMD=ON
#PCSX2_VERSION = v1.7.3648
#######################################################################################################################

#Version: Commits on Dec 24, 2022
PCSX2_VERSION = v1.7.3772
PCSX2_SITE = https://github.com/pcsx2/pcsx2.git
PCSX2_LICENSE = GPLv2 GPLv3 LGPLv2.1 LGPLv3
PCSX2_DEPENDENCIES = xserver_xorg-server alsa-lib freetype zlib libpng wxwidgets libaio portaudio libsoundtouch sdl2 libpcap yaml-cpp libsamplerate fmt libgtk3
PCSX2_LICENSE_FILES = COPYING.GPLv2

PCSX2_SITE_METHOD = git
PCSX2_GIT_SUBMODULES = YES
PCSX2_SUPPORTS_IN_SOURCE_BUILD = NO

## print version of core in PixL
define PCSX2_PRE_CONFIGURE
	echo "Pcsx2;pcsx2;${PCSX2_VERSION}" > $(TARGET_DIR)/recalbox/share_init/system/configs/pcsx2.corenames
endef
PCSX2_PRE_CONFIGURE_HOOKS += PCSX2_PRE_CONFIGURE

PCSX2_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release
PCSX2_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
PCSX2_CONF_OPTS += -DPCSX2_TARGET_ARCHITECTURES=x86_64
PCSX2_CONF_OPTS += -DCMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE
PCSX2_CONF_OPTS += -DwxWidgets_CONFIG_EXECUTABLE="$(STAGING_DIR)/usr/bin/wx-config"
PCSX2_CONF_OPTS += -DQT_BUILD=FALSE
PCSX2_CONF_OPTS += -DXDG_STD=TRUE
PCSX2_CONF_OPTS += -DDISABLE_PCSX2_WRAPPER=1
PCSX2_CONF_OPTS += -DPACKAGE_MODE=FALSE
PCSX2_CONF_OPTS += -DENABLE_TESTS=OFF
PCSX2_CONF_OPTS += -DUSE_SYSTEM_LIBS=AUTO
PCSX2_CONF_OPTS += -DDISABLE_ADVANCE_SIMD=OFF
PCSX2_CONF_OPTS += -DUSE_VTUNE=OFF
PCSX2_CONF_OPTS += -DUSE_DISCORD_PRESENCE=OFF
PCSX2_CONF_OPTS += -DLTO_PCSX2_CORE=ON
PCSX2_CONF_OPTS += -DUSE_ACHIEVEMENTS=ON
PCSX2_CONF_OPTS += -DX11_API=ON
PCSX2_CONF_OPTS += -DWAYLAND_API=OFF
PCSX2_CONF_OPTS += -DUSE_OPENGL=ON
PCSX2_CONF_OPTS += -DUSE_VULKAN=ON

define PCSX2_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 -D $(@D)/buildroot-build/pcsx2/pcsx2 $(TARGET_DIR)/usr/bin/PCSX2/pcsx2
	cp -pr $(@D)/buildroot-build/pcsx2/resources $(TARGET_DIR)/usr/bin/PCSX2
	mkdir -p $(TARGET_DIR)/recalbox/share_upgrade/bios/ps2

	# For Create a update online package
	mkdir -p $(BR2_EXTERNAL_RECALBOX_PATH)/package/pcsx2/update-resources/package/
	$(INSTALL) -D $(@D)/buildroot-build/pcsx2/pcsx2 \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/pcsx2/update-resources/package/pcsx2
	cp -pr $(@D)/buildroot-build/pcsx2/resources \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/pcsx2/update-resources/package
endef

# Hotkeys using evmapy
define PCSX2_EVMAPY
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/pcsx2/pcsx2.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef
PCSX2_POST_INSTALL_TARGET_HOOKS += PCSX2_EVMAPY

$(eval $(cmake-package))
