################################################################################
#
# pixL themes for Pegasus : https://github.com/pixl-os/shinretro
#
################################################################################

# last commit from pixL-master
# https://github.com/pixl-os/shinretro/tags
SHINRETRO_THEME_VERSION = v0.201.4
SHINRETRO_VERSION = a8a69d8cb33c987df5aad1680f719ae055820ff0
SHINRETRO_SITE = $(call github,pixl-os,shinretro,$(SHINRETRO_VERSION))

define SHINRETRO_INSTALL_TARGET_CMDS
	# create directories if not already done
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes/shinretro
	# copy theme files in share_init
	cp -r $(@D)/* $(TARGET_DIR)/recalbox/share_init/themes/shinretro
endef

$(eval $(generic-package))